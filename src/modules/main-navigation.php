	<nav id="main-nav" class="main-nav">
		<button id="hide-main-nav" class="hide-main-nav">Ukryj menu</button>
			<ul class="menu">
				<li><a href="./" <?php echo ($activePage == 'home') ? 'class="active-header-link"' : ''; ?>>Strona główna</a></li>
				<li><a href="./o-nas.php" <?php echo ($activePage == 'about') ? 'class="active-header-link"' : ''; ?>>O nas / Oferta</a></li>
				<li><a href="./oferta-insert.php" <?php echo ($activePage == 'insert') ? 'class="active-header-link"' : ''; ?>>Oferta InsERT</a></li>
				<li><a id="nav-show-contact" href="javascript:;">Kontakt</a></li>		
			</ul>
			<div class="main-nav-contact">
				<p class="main-nav-contact-title">Adres:</p>
				<p>ul. <span> Józefa Włodka 16B, Grudziądz</span></p>
				<br>
				<p class="main-nav-contact-title">Szybki kontakt:</p>
				<p>tel. <span>(56) 64 31 500</span></p>
				<p>tel.kom. <span>508 282 511</span></p>
				<p class="email-show-contact2">salonexe@gmail.com</p>
				<br>
				<p class="main-nav-contact-title">Otwarte:</p>
				<p>pon.-piąt. : <span>10.00</span> – <span>18.00</span></p>
				<p>sobota : <span>10.00</span> – <span>13.00</span></p>
				<p>niedziela : <span>Nieczynny</span></p>
			</div>		
			<div class="main-nav-links">
				<p class="main-nav-links-title">Linki do stron oraz programów dzięki którym życie z twoim PC-etem stanie się łatwiejsze!</p>
				<a target="_blank" href="http://www.microsoft.com/">Program antywirusowy</a>
				<a target="_blank" href="http://support.amd.com/">Sterowniki do kart Graficznych ATI</a>
				<a target="_blank" href="http://www.nvidia.pl/">Sterowniki do Kart Graficznych Nvidia</a>
				<a target="_blank" href="http://www.teamviewer.com/">Teamviewer</a>
			</div>
			<div class="nav-recommend">
				<p>Polecamy:</p>
				<a target="_blank" href="https://www.insert.com.pl/programy_dla_firm.html">
					<img src="img/insert.png" alt="Logo firmy Insert">				
				</a>
				<a target="_blank" href="http://www.novitus.pl/">
					<img src="img/novitus.jpg" alt="Logo firmy Novitu">
				</a>
				<a target="_blank" href="http://www.wapro.pl/WAPRO">
					<img src="img/wapro.gif" alt="Logo firmy Wapro">
				</a>
				<a target="_blank" href="https://get.teamviewer.com/qf4pss4">
					<img src="img/teamviewer.svg" alt="Logo Teamviewer">
				</a>
				<a target="_blank" href="http://www.insoft.com.pl/pc-market">
					<img src="img/insoft.jpg" alt="Logo Insof">
				</a>	
				<a target="_blank" href="https://www.eltrox.pl/">
					<img src="img/eltrox.png" alt="Logo firmy eltrox">
				</a>				
				</div>
		</nav>