<section class="offer section">

	<div class="container">
		<header class="section-header">
			<h3>Oferta</h3>
			<h4>Krótko o naszych usługach</h4>
		</header>
	</div>

	<div class="container">
		<ul class="offer-items">
			<li>		
				<span class="fa fa-desktop"></span>
				<p>Pomoc przy doborze zestawów komputerowych i laptopów.</p>
			</li>
			<li>
				<span class="fa fa-cog"></span>
				<p>Profesjonalny serwis gwarancyjny i pogwarancyjny laptopów, komputerów stacjonarnych, monitorów LCD, drukarek i urządzeń przenośnych.</p>
			</li>
			<li>
				<span class="fa fa-file-alt"></span>
				<p>Audyt licencyjny oprogramowania komputerowego.</p>
			</li>
			<li>		
				<span class="fa fa-list-alt"></span>
				<p>Usługi outsourcingowe dla firm i instytucji.</p>
			</li>
			<li>			
				<span class="fa fa-life-ring"></span>
				<p>Wsparcie techniczne dla produktów zakupionych w innych firmach.</p>
			</li>
			<li>
				<span class="fa fa-shield-alt"></span>
				<p>Usługi związane z monitoringiem oraz systemami alarmowymi.</p>
			</li>
		</ul>
		<div class="links-offer-container">
			<a class="offer-more" href="./o-nas.php">Zobacz całą ofertę</a>
			<a class="insert-offer-more" href="./oferta-insert.php">Zobacz ofertę firmy InsERT</a>
		</div>
	</div>

</section>