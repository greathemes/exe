<div>
	<div class="item-header">
		<img src="img/insert/gestor-gt.png" alt="Gestor-GT logo">
		<h4>Gestor GT</h4>

		<p>Gestor GT to wygodny i elastyczny system wspomagający budowanie trwałych relacji z klientami. Skutecznie wspiera różnego rodzaju działania handlowe i marketingowe, zapewniając wydajność i komfort pracy. Jego wszechstronność i możliwość dostosowania do własnych potrzeb sprawiają, że program może być wykorzystywany w firmach o dowolnej specjalizacji.</p>

		<p>Gestor GT przeznaczony jest dla wszystkich tych, którzy w centrum zainteresowania stawiają klienta. Sprawdza się m.in. w dziale handlowym, marketingu, dziale obsługi klienta, sekretariacie itp. Jest też przydatnym narzędziem dla menedżerów. </p>

		<p>Gestor GT w czytelny sposób udostępnia wszystkie zgromadzone w systemie dane dotyczące klientów. Pozwala starannie wyselekcjonować grupę docelową, dzięki czemu możliwe jest lepsze dostosowanie działań do konkretnych osób. Ułatwia podejmowanie trafnych decyzji oraz przygotowanie odpowiednich strategii sprzedaży na podstawie różnych zestawień. A wszystko to za pomocą kilku ruchów. Program został skonstruowany tak, by zaoszczędzić czas i zapewnić maksymalną efektywność przy stosunkowo niskim nakładzie pracy.</p>

		<p>Gestor GT jest częścią linii InsERT GT. Może działać samodzielnie lub współpracować z systemem sprzedaży Subiekt GT. Dzięki ścisłemu powiązaniu tych programów możliwe jest korzystanie ze wspólnej kartoteki kontrahentów i towarów, wraz ze wszystkimi poziomami cen, rabatami i promocjami. Integracja z Subiektem GT umożliwia także łatwe wystawianie dokumentów handlowych i obsługę wielu magazynów.</p>
	</div>
	
	<div class="item-footer">
	<h4>Najważniejsze zalety Gestora GT:</h4>

	<ul>
		<li>szybki dostęp do wszystkich informacji dotyczących klientów: działań, wiadomości, szans sprzedaży, dokumentów wystawionych w Subiekcie GT;</li>
		<li>planowanie i rejestrowanie w systemie działań takich jak: spotkania, zadania, telefony, listy, faksy i rozmowy internetowe;</li>
		<li>łatwe wyszukiwanie klienta z dowolnego miejsca w programie;</li>
		<li>kalendarz pokazujący zakres i termin czynności do wykonania (w widoku miesięcznym, tygodniowym i dziennym) wraz z funkcją przypominania;;</li>
		<li>wbudowany moduł poczty elektronicznej – automatyczne przypisywanie e-maili do odpowiednich klientów, możliwość tworzenia wiadomości zbiorczych (mailing) na podstawie zdefiniowanych szablonów oraz pól autotekstu;</li>
		<li>dostęp do pełnej historii kontaktów z klientem (działania, wiadomości e-mail, wystawione dokumenty, wiarygodność płatnicza, data ostatniego kontaktu, nazwisko opiekuna klienta, wysokość udzielonych rabatów itd.);</li>
		<li>możliwość przydzielania zadań oraz sprawdzania postępu prac poszczególnych pracowników;</li>
		<li>liczne zestawienia (m.in. lejek sprzedaży, prognozowanie sprzedaży, skuteczność sprzedawców, aktywność użytkowników itp.), także generowane na podstawie informacji zawartych w Subiekcie GT (np. sprzedaż wg asortymentu, raport o towarach);</li>
		<li>możliwość dostosowania do własnych potrzeb (m.in. dodawanie własnych zestawień, dostosowanie wydruków, edytowalne słowniki, pola własne, pasek szybkiego dodawania obiektów);</li>
		<li>powiązania – możliwość tworzenia relacji między obiektami, takimi jak klienci, szanse sprzedaży, działania i wiadomości;</li>
		<li>biblioteka dokumentów;</li>
		<li>schowek, w którym można przechowywać różne obiekty (np. zadania, wiadomości, klientów, szanse sprzedaży) do późniejszego wykorzystania;</li>
		<li>obsługa walut (w tym pobieranie kursów walut przez Internet);</li>
		<li>wygodny system notatek;</li>
		<li>strona główna zawierająca listę zadań dla użytkownika oraz prognozę wartości sprzedaży liczoną na podstawie otwartych szans sprzedaży;</li>
		<li>możliwość otwierania modułów w wielu zakładkach;</li>
		<li>praca sieciowa wielu użytkowników programu.</li>
	</ul>
</div>
</div>