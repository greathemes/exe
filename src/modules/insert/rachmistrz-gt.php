<div>
	<div class="item-header">
		<img src="img/insert/rachmistrz-gt.png" alt="Rachmistrz-GT logo">
		<h4>Rachmistrz GT</h4>

		<p>Rachmistrz GT jest kolejną wersją znanych w Polsce programów firmy InsERT: Rachmistrza 4 i Rachmistrza dla Windows. To sprawny system wyposażony w najnowsze rozwiązania interfejsowe czyniące go programem bardzo ergonomicznym i przyjaznym dla użytkownika.</p>

		<p>Rachmistrz GT jest częścią linii InsERT GT, która zawiera również Subiekta GT – system obsługi sprzedaży dla małych i średnich firm, Rewizora GT – program do prowadzenia księgi handlowej, Gratyfikanta GT – system kadrowo-płacowy oraz Gestora GT – system zarządzania relacjami z klientami. Motor bazy danych zastosowany w programach linii InsERT GT to Microsoft SQL Server 2008 R2, który w bezpłatnej wersji Express Edition jest dostarczany wraz z systemem.</p>
	</div>
	
	<div class="item-footer">
	<h4>Najważniejsze cechy Rachmistrza GT:</h4>

	<ul>
		<li>Prowadzenie księgi przychodów i rozchodów lub ewidencji podatku zryczałtowanego (w zależności od wybranej formy księgowości);</li>
		<li>prowadzenie ewidencji VAT: zakupów, sprzedaży w tym ewidencjonowanie transakcji unijnych; możliwość wprowadzania sprzedaży nieudokumentowanej rozliczanej strukturą zakupów;</li>
		<li>prowadzenie ewidencji danych pojazdów wykorzystywanych do celów służbowych, rachunków związanych z eksploatacją pojazdów oraz ewidencji przebiegu pojazdów;</li>
		<li>prowadzenie ewidencji środków trwałych oraz wartości niematerialnych i prawnych (naliczanie i księgowanie amortyzacji, wydruk planu amortyzacji), a także prowadzenie ewidencji wyposażenia i remanentów;</li>
		<li>prowadzenie kartoteki kontrahentów oraz wspólników (indywidualne parametry dotyczące rozliczeń z ZUS, automatyczne księgowanie w koszty lub odliczanie od dochodu składek ZUS i innych funduszy, obliczanie i dokumentowanie odpowiednim raportem zaliczki na podatek dochodowy);</li>
		<li>rozbudowana obsługa wynagrodzeń: ewidencja osobowa, umów cywilnoprawnych oraz umów o pracę; wystawianie rachunków do umów cywilnoprawnych oraz wypłat (w tym możliwość automatycznego wyliczania na podstawie zdefiniowanych schematów wynagrodzeń);</li>
		<li>naliczanie, wydruk oraz pełna obsługa wysyłki elektronicznej deklaracji skarbowych (wraz z niezbędnymi załącznikami): VAT-7, VAT-7K, VAT-7D,VAT-UE, PIT-11, PIT-36, PIT-36L, PIT-37, PIT-40, PIT-4R, PIT-28, PIT-8AR;</li>
		<li>Obsługa korekt deklaracji skarbowych z wysyłką elektroniczną;</li>
		<li>wystawianie deklaracji ZUS (DRA, RCA, RZA) oraz możliwość eksportu do programu Płatnik ZUS;</li>
		<li>integracja z systemem obsługi sprzedaży Subiekt GT oraz możliwość współpracy (przez opcję Komunikacja) z innymi systemami (np. Subiekt dla Windows, Subiekt 5 EURO, mikroSubiekt dla Windows).</li>
	</ul>

	<p>Rachmistrz GT dostępny jest również w specjalnej ofercie przygotowanej dla biur rachunkowych i doradców podatkowych, która umożliwia zakup programu w promocyjnym zestawie.</p>
</div>
</div>