<div>
	<div class="item-header">
		<img src="img/insert/subiekt-nexo.png" alt="Subiekt Nexo logo">
		<h4>Subiekt Nexo</h4>

		<p>Subiekt nexo to program kompleksowo wspomagający obsługę sprzedaży w małych i średnich firmach – sklepach, hurtowniach, warsztatach, zakładach usługowych itp. Przyjazny, intuicyjny interfejs ułatwia i przyspiesza pracę, a bogata funkcjonalność zaspokoi potrzeby nowoczesnych przedsiębiorców, bez względu na branżę.</p>

		<p>Subiekt nexo zapewnia obsługę wszystkich typów dokumentów handlowych (szczególną zaletą jest zaawansowany system zamówień) i sprawne zarządzanie gospodarką magazynową. Posiada rozbudowaną część finansową, dzięki której możliwe jest m.in. wykonywanie operacji kasowych, bankowych czy tworzenie raportów kasowych. Moduły rozliczeniowe umożliwiają ewidencjonowanie rozrachunków, działań windykacyjnych i tabeli kursów walut. Kartotekowa część systemu pozwala prowadzić zaawansowaną ewidencję asortymentu, klientów, urządzeń zewnętrznych, instytucji, wspólników i pracowników.</p>

		<p>Subiekt to od lat najczęściej wybierana przez polskich przedsiębiorców marka systemu sprzedaży. Subiekt nexo jest kolejną wersją tego popularnego programu. Został zaprojektowany ze szczególną dbałością o wygodę użytkownika, dzięki czemu prowadzenie biznesu staje się prostsze.</p>

		<p>Subiekt nexo jest częścią InsERT nexo – zintegrowanego pakietu, w którego skład wchodzą również systemy księgowe: Rachmistrz nexo (księga przychodów i rozchodów oraz ewidencja podatku zryczałtowanego) i Rewizor nexo (pełna księgowość)</p>
	</div>
	
	<div class="item-footer">
	<h4>Najważniejsze możliwości Subiekta nexo:</h4>

	<ul>
		<li>pełna obsługa wszystkich typów dokumentów handlowych i magazynowych;</li>
		<li>pełny rozdział dokumentów magazynowych i handlowych, pojedyncze lub zbiorcze generowanie faktur do dokumentów WZ; korekty dokumentów magazynowych; automatyczne generowanie dokumentu WZ do faktury;</li>
		<li>Faktura pro forma; faktura sprzedaży z wielu magazynów;</li>
		<li>rozbudowany system zamówień – każda pozycja dokumentu może mieć indywidualny termin realizacji, również wcześniejszy niż termin realizacji całego dokumentu;</li>
		<li>kartoteka asortymentu – towarów, usług, kompletów i opakowań zwrotnych; działy sprzedaży w kartotece;</li>
		<li>kartoteka klientów obejmująca również informacje typu CRM;</li>
		<li>prowadzenie elastycznej polityki cenowej – możliwość przypisywania zdefiniowanych cenników do poszczególnych klientów; cena, domyślny i maksymalny rabat pozycji cennika mogą być określane w zależności od jednostki miary i wysokości progu sprzedaży;</li>
		<li>obsługa przedpłat, płatności kartą, płatności za pobraniem, sprzedaży kredytowanej, cesji na innego płatnika, zaliczek pracowników na zakup;</li>
		<li>kompleksowa obsługa kasy z rejestrowaniem operacji gotówkowych i bezgotówkowych (płatności kartą płatniczą, bony), operacje kasowe z odłożonym skutkiem (niewykonane);</li>
		<li>rozbudowana obsługa rachunków bankowych (przelewy standardowe, do ZUS, podatkowe, operacje na rachunku, opłaty prowizyjne, wyciągi);</li>
		<li>bsługa bankowości elektronicznej za pomocą wymiany plików, a także online (wybrane banki);</li>
		<li>prowadzenie rozrachunków (należności i zobowiązań), rozliczenia wielowalutowe, wezwania do zapłaty, noty odsetkowe;</li>
		<li>rozbudowana obsługa urządzeń zewnętrznych (kasy fiskalne, drukarki fiskalne, wagi etykietujące, czytniki kodów kreskowych);</li>
		<li>automatyczna synchronizacja zmian asortymentu w kartotece i urządzeniach zewnętrznych;</li>
		<li>pełna integracja z systemami finansowo-księgowymi: Rachmistrz nexo i Rewizor nexo.</li>
	</ul>
</div>
</div>