<div>
	<div class="item-header">
		<img src="img/insert/gratyfikant-gt.png" alt="Gratyfikant-GT logo">
		<h4>Gratyfikant GT</h4>

		<p>Gratyfikant GT jest nowoczesnym systemem kadrowo-płacowym, następcą programu Gratyfikant 3. Program polecamy osobom, które w małych i średnich firmach zajmują się sprawami kadrowo-płacowymi, a także biurom rachunkowym.</p>

		<p>Gratyfikant GT posiada wiele rozbudowanych funkcji niezbędnych w dziale kadr i płac. Umożliwia prowadzenie ewidencji osobowej w firmie, wystawianie różnego rodzaju umów, ewidencjonowanie wypłat i rachunków, ponadto obsługuje Zakładowy Fundusz Świadczeń Socjalnych, ułatwia wystawianie deklaracji skarbowych (m.in.: PIT-4, PIT-4R, PIT-8AR, PIT-11/8B, PIT-36, PIT-36L, PIT-37, PIT-40) oraz ZUS (m.in.: RCA, RZA, RSA, DRA). System dostosowany jest do obowiązujących przepisów. Gratyfikant GT współpracuje z programem Płatnik.</p>

		<p>Gratyfikant GT wchodzi w skład systemu InsERT GT, zintegrowany został z programami księgowymi tej linii – księgą przychodów i rozchodów – Rachmistrzem GT i systemem finansowo-księgowym – Rewizorem GT. Możliwe jest przeniesienie do niego danych zgromadzonych w Gratyfikancie 3 oraz mikroGratyfikancie GT, a także w systemie Płatnik.</p>
	</div>
	
	<div class="item-footer">
	<h4>Poniżej przedstawiamy listę najważniejszych możliwości programu:</h4>

	<ul>
		<li>unikalny i elastyczny model wynagrodzeń, dający bardzo duże możliwości w zakresie tworzenia, zarządzania i naliczania wynagrodzeń dla pracowników;</li>
		<li>rozbudowana ewidencja pracowników (badania lekarskie, kursy bhp, nagrody, kary, historia zatrudnienia, podział na grupy, przypisywanie cech, zdjęcia pracownika i wiele innych);</li>
		<li>ewidencja umów o pracę z bardzo elastycznym mechanizmem tworzenia zaawansowanych systemów wynagrodzeń (wiele definicji wypłat w jednej umowie, globalne lub indywidualne składniki płacowe);</li>
		<li>planowanie czasu pracy pracownika w umowach o pracę (zmiany kalendarza pracy w czasie trwania umowy, określanie wyjątków), wydruki kalendarzy miesięcznych i rocznych;</li>
		<li>ewidencja umów zleceń, o dzieło oraz kontraktów menedżerskich (m.in. kwotowe koszty uzyskania przychodów); definiowanie elastycznych harmonogramów wypłacania rachunków dla umów cywilnoprawnych;</li>
		<li>rejestracja faktycznego czasu pracy pracownika (nadgodziny, godziny nocne, godziny absencji, rozliczanie miesięczne lub okresowe, np. co 3 miesiące);</li>
		<li>ewidencja nieobecności w pracy (absencje chorobowe, urlopowe i wiele innych) dla umów o pracę i umów cywilnoprawnych (wybrane absencje);</li>
		<li>rozbudowane składniki płacowe (naliczenia, potrącenia, składniki parametryczne, składniki definiowalne, składniki automatyczne, duże możliwości parametryzacji);</li>
		<li>definicje list płac i zestawów płacowych, ułatwiające i automatyzujące naliczanie comiesięcznych wynagrodzeń; rozliczanie ewidencji czasu pracy, akordów, prowizji, naliczeń, potrąceń i potrąceń komorniczych w dowolnej liście płac; rozbudowany analizator wypłat;.</li>
		<li>naliczanie i drukowanie deklaracji skarbowych (PIT-4, PIT-4R,PIT-8AR, PIT-11/8B, PIT-36, PIT-36L, PIT-37, PIT-40); generowanie danych do deklaracji zgłoszeniowych i rozliczeniowych ZUS.</li>
	</ul>

	<p>Funkcjonalność Gratyfikanta GT można wzbogacić, instalując specjalne pakiety rozszerzeń: niebieski PLUS dla InsERT GT, zielony PLUS dla InsERT GT i czerwony PLUS dla InsERT GT. Natomiast użytkownikom, którzy wymagają nietypowych rozwiązań, dostosowanych do specyficznych potrzeb firmy, polecamy dodatek Sfera dla Gratyfikanta GT.</p>

	<p>Gratyfikant GT dostępny jest również w specjalnej ofercie przygotowanej dla biur rachunkowych i doradców podatkowych, która umożliwia zakup programu w promocyjnym zestawie.</p>
	</div>
</div>