<div>
	<div class="item-header">
		<img src="img/insert/subiekt-gt.png" alt="Subiekt-GT logo">
		<h4>Subiekt GT</h4>

		<p>Subiekt GT to nowoczesny system sprzedaży stworzony z myślą o firmach, które poszukują sprawnego narzędzia wspomagającego całościową obsługę działu handlowego, sklepu, punktu usługowego, rzemieślniczego, itp.a.</p>

		<p>Subiekt GT jest kolejną wersją znanych w Polsce systemów firmy InsERT: Subiekta 4, Subiekta 5 i Subiekta dla Windows. Dzięki bogatej funkcjonalności i prostej, intuicyjnej obsłudze, program sprawdza się w każdej sytuacji. Zastosowane technologie i rozwiązania biznesowe sprawiają, że Subiekt to najczęściej w Polsce wybierany program do wspomagania sprzedaży.</p>

		<p>Subiekt GT jest sprawnym i szybkim systemem wyposażonym w najnowsze rozwiązania interfejsowe czyniące go programem bardzo ergonomicznym i przyjaznym dla użytkownika. Jednocześnie jego wszechstronność i możliwości konfiguracji czynią go produktem niemal dla każdego.</p>

		<p>Subiekt GT jest częścią linii InsERT GT, która zawiera również Rachmistrza GT – program do prowadzenia księgowości w formie książki przychodów i rozchodów lub ewidencji podatku zryczałtowanego, Rewizora GT – program do prowadzenia księgi handlowej, Gratyfikanta GT – system kadrowo-płacowy oraz Gestora GT – system zarządzania relacjami z klientami. Motor bazy danych zastosowany w programach linii InsERT GT to Microsoft SQL Server 2008 R2, który w bezpłatnej wersji Express Edition jest dostarczany wraz z systemem.</p>
	</div>
	
	<div class="item-footer">
	<h4>Podstawowe możliwości Subiekta GT:</h4>

	<ul>
		<li>pełna obsługa wszystkich typów dokumentów handlowych i magazynowych;</li>
		<li>prowadzenie kartoteki kontrahentów, obejmującej również informacje typu CRM; możliwość definiowania rubryk z dodatkowymi informacjami;</li>
		<li>prowadzenie kartoteki towarów, usług, kompletów i opakowań zwrotnych z rozbudowaną kalkulacją cen (10 poziomów cen sprzedaży, w tym ceny walutowe);</li>
		<li>pełny rozdział dokumentów magazynowych i handlowych, pojedyncze lub zbiorcze generowanie faktur do dokumentów WZ itp.; automatyczne generowanie dokumentu WZ do faktury;</li>
		<li>obsługa przedpłat, płatności kartą kredytową oraz sprzedaży kredytowanej;</li>
		<li>obsługa zamówień z rezerwacją towarów oraz fakturami zaliczkowymi;</li>
		<li>rozbudowana obsługa rachunków bankowych (złotówkowych lub walutowych) z operacjami wpłaty, wypłaty i transferu (plus opłaty prowizyjne, wyciągi, itp.);</li>
		<li>pełna obsługa rozrachunków (należności i zobowiązań), w tym rozrachunków wielowalutowych;</li>
		<li>wiele różnorodnych zestawień, raportów i analiz; możliwość tworzenia własnych raportów;</li>
		<li>rozbudowana obsługa urządzeń zewnętrznych (kasy fiskalne, drukarki fiskalne, wagi etykietujące, czytniki kodów paskowych);</li>
		<li>pełna integracja z systemem zarządzania relacjami z klientami (CRM) Gestor GT;</li>
		<li>pełna integracja z komputerową księgą przychodów i rozchodów Rachmistrz GT oraz systemem finansowo-księgowym Rewizor GT.</li>
	</ul>

	<p>Użytkownikom, którzy chcieliby dopasować Subiekta GT do specyficznych potrzeb, polecamy Sferę dla Subiekta GT – dodatek umożliwiający rozbudowę systemu sprzedaży. Funkcjonalność Subiekta GT rozszerzą również specjalne pakiety – niebieski PLUS dla InsERT GT, zielony PLUS dla InsERT GT i czerwony PLUS dla InsERT GT.</p>

	<p>Integracja programu z mobilnym Subiektem, oscGT i Sello stwarza całkiem nowe możliwości, sprzyjając poszerzeniu rynku zbytu. Towary z Subiekta GT mogą być sprzedawane w terenie (mobilny Subiekt) oraz w internecie – zarówno w sklepie internetowym (oscGT), jak i na aukcjach (Sello).</p>
</div>
</div>