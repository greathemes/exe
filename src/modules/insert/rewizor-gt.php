<div>
	<div class="item-header">
		<img src="img/insert/rewizor-gt.png" alt="Rewizor-GT logo">
		<h4>Rewizor GT</h4>

		<p>Rewizor GT to profesjonalny system finansowo-księgowy dla małych i średnich przedsiębiorstw. Program stworzony jest w oparciu o Ustawę o Rachunkowości oraz dostosowany do wymogów Unii Europejskiej. Przeznaczony zarówno dla samodzielnych księgowych jak i biur rachunkowych. Rewizor GT posiada pełną gamę funkcji potrzebnych do sprawnego prowadzenia księgi handlowej: od administrowania planami kont, poprzez dekretację i księgowanie, zarządzanie rozrachunkami, po obsługę środków trwałych.</p>

		<p>Rewizor GT jest programem niezwykle prostym w obsłudze, o nowoczesnym interfejsie zapewniającym najwyższą ergonomię i wygodę pracy. Dzięki intuicyjnemu kreatorowi wdrożeniowemu początek pracy z programem nie sprawi trudności nawet mniej wprawnym użytkownikom.</p>

		<p>Rewizor GT to część składowa systemu InsERT GT – w pełni zintegrowanego pakietu programów do prowadzenia firmy z sektora MSP, zawierającego także program do obsługi sprzedaży – Subiekt GT, system kadrowo-płacowy – Gratyfikant GT oraz system zarządzania relacjami z klientami – Gestor GT. W Rewizorze GT możliwa jest podstawowa obsługa płac dzięki dołączanemu bezpłatnie programowi mikroGratyfikant GT.</p>
	</div>
	
	<div class="item-footer">
	<h4>Podstawowe cechy Rewizora GT:</h4>

	<ul>
		<li>elastyczne zarządzanie planami kont: możliwość generowania wzorcowego planu kont, przeniesienia z innego roku obrotowego, automatycznie tworzenie kont kartotekowych przez podłączanie kartotek, obsługa kont pozabilansowych;</li>
		<li>rozbudowana dekretacja: podział dokumentów księgowych na dekrety i dokumenty zaksięgowane, automatyzacja wykonywanych czynności (tworzenie i rozliczanie rozrachunków, generowanie zapisów VAT), obsługa dekretów walutowych;</li>
		<li>zaawansowane zarządzanie rozrachunkami (tworzenie i rozliczanie); ścisłe powiązanie rozrachunków z zapisami na kontach; pełna historia rozrachunków; rozrachunki wspólne z Subiektem GT;</li>
		<li>ewidencja i rozliczanie podatku VAT w pełni zgodne z aktualnie obowiązującą ustawą: ewidencjonowanie transakcji VAT krajowych (dostawy i nabycia), wewnątrzwspólnotowych (WNT i WDT) oraz pozaunijnych (eksport towarów, import towarów); automatyczne wyliczanie deklaracji VAT-7/7K – rozliczanie miesięczne lub kwartalne;</li>
		<li>automatyczne tworzenie dekretów księgowych za pomocą dekretacji kontekstowej i importu; rozbudowane schematy importu; szczególne wsparcie dla użytkowników pracujących jednocześnie na Subiekcie GT i Rewizorze GT (moduł Dokumenty do dekretacji);</li>
		<li>funkcje specjalnie przygotowane dla biur rachunkowych: obsługa dowolnej liczby podmiotów gospodarczych, przenoszenie planów kont z innych podmiotów, zbiorcze operacje na licencjach, import dokumentów z pliku wygenerowanego przez zdalnego Subiekta GT, archiwizacja wszystkich podmiotów jednocześnie i inne;</li>
		<li>ewidencja środków trwałych i wartości niematerialnych i prawnych oraz operacji z nimi związanych;</li>
		<li>definiowanie i wyliczanie szeregu sprawozdań i zestawień finansowych oraz deklaracji skarbowych;</li>
		<li>tworzenie wydruków graficznych (wszystkie) i tekstowych (główne); zaawansowane zarządzanie wydrukami graficznymi z poziomu programu;</li>
		<li>motor bazy danych Microsoft SQL Server 2008 R2 zapewniający wysoką wydajność i bezpieczeństwo pracy;</li>
		<li>praca w sieci komputerowej lub na pojedynczym stanowisku, współpraca z innymi programami firmy InsERT.</li>
	</ul>

	<p>Rewizor GT dostępny jest również w specjalnej ofercie przygotowanej dla biur rachunkowych i doradców podatkowych, która umożliwia zakup programu</p>
</div>
</div>