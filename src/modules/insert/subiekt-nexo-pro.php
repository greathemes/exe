<div>
	<div class="item-header">
		<img src="img/insert/subiekt-nexo-pro.png" alt="Subiekt Nexo PRO logo">
		<h4>Subiekt Nexo PRO</h4>

		<p>Subiekt nexo PRO to program wspomagający obsługę sprzedaży w małych i średnich firmach. Jest to rozszerzona wersja Subiekta nexo – oprócz pełnej funkcjonalności standardowego systemu zawiera wiele dodatkowych rozwiązań, które spełnią nawet bardzo nietypowe wymagania.</p>

		<p>Subiekt nexo PRO pozwala na obsługę przedsiębiorstwa wielooddziałowego. Umożliwia zaawansowane nadawanie cen towarom poprzez cenniki główne i dodatkowe. Wyróżnia się rozbudowaną obsługą zamówień – dla wersji PRO stworzono m.in. widoki robocze (Asortyment na zamówieniach do obsługi pozycji zamówienia, Asortyment na wyczerpaniu, Asortyment na zleceniach), które ułatwiają logistykę zamówień zarówno w przypadku klientów, jak i dostawców.</p>

		<p>Subiekt nexo PRO charakteryzuje się też większą otwartością i elastycznością. Pozwala np. dodawać pola własne do różnych elementów programu, a także własne raporty i wzorce wydruku. Dzięki Sferze dla Subiekta możliwe jest tworzenie indywidualnych rozwiązań dostosowujących system do specyficznych potrzeb firmy.</p>
	</div>
	
	<div class="item-footer">
	<h4>Podstawowe możliwości Subiekta nexo PRO:</h4>

	<ul>
		<li>pełna obsługa wszystkich typów dokumentów handlowych i magazynowych;</li>
		<li>pełny rozdział dokumentów magazynowych i handlowych, pojedyncze lub zbiorcze generowanie faktur do dokumentów WZ; korekty dokumentów magazynowych; automatyczne generowanie dokumentu WZ do faktury; faktura pro forma; faktura sprzedaży z wielu magazynów;</li>
		<li>rozbudowany system zamówień – każda pozycja dokumentu może mieć indywidualny termin realizacji, również wcześniejszy niż termin realizacji całego dokumentu;</li>
		<li>kartoteka asortymentu – towarów, usług, kompletów i opakowań zwrotnych; działy sprzedaży w kartotece;</li>
		<li>kartoteka klientów obejmująca również informacje typu CRM;
		prowadzenie elastycznej polityki cenowej – możliwość przypisywania zdefiniowanych cenników do poszczególnych klientów; cena, domyślny i maksymalny rabat pozycji cennika mogą być określane w zależności od jednostki miary i wysokości progu sprzedaży;</li>
		<li>obsługa przedpłat, płatności kartą, płatności za pobraniem, sprzedaży kredytowanej, cesji na innego płatnika, zaliczek pracowników na zakup; Kompleksowa obsługa kasy z rejestrowaniem operacji gotówkowych i bezgotówkowych (płatności kartą płatniczą, bony), operacje kasowe z odłożonym skutkiem (niewykonane);</li>
		<li>rozbudowana obsługa rachunków bankowych (przelewy standardowe, do ZUS, podatkowe, operacje na rachunku, opłaty prowizyjne, wyciągi); obsługa bankowości elektronicznej za pomocą wymiany plików, a także online (wybrane banki);</li>
		<li>prowadzenie rozrachunków (należności i zobowiązań), rozliczenia wielowalutowe, wezwania do zapłaty, noty odsetkowe;</li>
		<li>rozbudowana obsługa urządzeń zewnętrznych (kasy fiskalne, drukarki fiskalne, wagi etykietujące, czytniki kodów kreskowych);</li>
		<li>automatyczna synchronizacja zmian asortymentu w kartotece i urządzeniach zewnętrznych;</li>
		<li>pełna integracja z systemami finansowo-księgowymi: Rachmistrz nexo i Rewizor nexo.</li>
	</ul>	

	<h4>Dodatkowe rozwiązania dostępne w wersji PRO:</h4>

	<ul>
		<li>Sfera dla Subiekta nexo – możliwość tworzenia własnych rozwiązań;</li>
		<li>zaawansowane pola własne do obiektów systemu; definiowanie własnych raportów i wydruków;</li>
		<li>korekta kosztu dostaw;</li>
		<li>sprzedaż z dostaw, rezerwacja dostaw w dokumentach, zakup z rozbiciem przyjęcia na partie, rejestracja rozbieżności w przyjęciach magazynowych;</li>
		<li>możliwość realizowania pojedynczego zamówienia wieloma wydaniami, a jedno wydanie może obejmować pozycje z różnych zamówień;</li>
		<li>widok roboczy „Asortyment na zamówieniach do obsługi pozycji zamówienia”;</li>
		<li>widok roboczy „Asortyment na wyczerpaniu” oraz „Asortyment na zleceniach” do wydajnej i efektywnej pracy z zamówieniami do dostawców;</li>
		<li>możliwość zdefiniowania wielu dostawców asortymentu z wyróżnionym jednym głównym dostawcą i producentem (każdemu dostawcy można przypisać dla danego asortymentu zarówno zestaw jego indywidualnych symboli, jak i terminy dostawy);</li>
		<li>rozszerzone możliwości płatności odroczonych – rozbicie na raty</li>
		<li>możliwość zdefiniowania cenników dodatkowych (dotyczących części cennika głównego, można je ograniczać czasowo);</li>
		<li>obsługa zamienników – prostych zbiorów towarów bądź usług, które na dokumentach można wymienić za pomocą specjalnej funkcji pomiędzy elementami z tego samego zbioru;</li>
		<li>obsługa oddziałów firmy (z każdym oddziałem jest związany cennik główny, który jest domyślny dla każdego wystawianego do niego dokumentu handlowego);</li>
		<li>możliwość podzielenia rozrachunku na raty (rozrachunek może być podzielony na wiele kwot cząstkowych z określeniem terminu płatności dla każdej z nich);</li>
		<li>rozliczenia wielowalutowe (np. EUR – USD);</li>
		<li>sesje kasowe – narzędzie do weryfikacji pracy kasjera (w momencie zamykania sesji pogram sam wylicza, czy stan środków pieniężnych jest zgodny z operacjami zarejestrowanymi w systemie);</li>
		<li>Sesje rozliczeniowe – narzędzie do lepszego zarządzania rozliczeniami.</li>
	</ul>	
</div>
</div>