<div>
	<div class="item-header">
		<img src="img/insert/biuro-gt.png" alt="Biuro-GT logo">
		<h4>Biuro GT</h4>

		<p>Biuro GT to system przeznaczony dla księgowych i kadrowych zatrudnionych w biurze rachunkowym oraz dla doradców podatkowych. Jest on uzupełnieniem programów księgowych i kadrowo-płacowych z linii InsERT GT (Rachmistrz GT, Rewizor GT, Gratyfikant GT, mikroGratyfikant GT) oraz narzędziem integrującym operacje wykonywane w tych programach.</p>

		<p>Biuro GT umożliwia sprawną obsługę dużej liczby podmiotów bez konieczności wyboru wszystkich obsługiwanych podmiotów i odszukiwania odpowiedniego modułu administracyjnego. System automatyzuje wykonywanie operacji powtarzalnych, koniecznych do przeprowadzenia w każdym z obsługiwanych podmiotów.</p>

		<p>W systemie możliwe jest także automatyczne modyfikowanie najważniejszych parametrów programów księgowych i kadrowych. Jest to program intuicyjny w obsłudze, którego zadaniem jest przede wszystkim znaczne przyspieszenie pracy osób zatrudnionych w biurze rachunkowym.</p>

		<p>Biuro GT zawiera mechanizmy pozwalające na łatwe harmonogramowanie pracy biura i kontrolowanie niektórych operacji wykonywanych przez biuro rachunkowe. Program ułatwia zarządzanie czynnościami administracyjnymi na wielu bazach danych – przede wszystkim przy archiwizacji i konwersji baz danych. System generuje także odpowiednie raporty ułatwiające rozliczanie pomiędzy biurem rachunkowym a jego klientami.</p>

		<p>Funkcjonalność Biura GT ogólnie można podzielić na dwie płaszczyzny działania. Pierwsza to wspomaganie pracy z programami linii InsERT GT, druga to funkcje, które ułatwiają planowanie rozmaitych zadań i kontrolują pracę całego biura.</p>
	</div>
	
	<div class="item-footer">
		<h4>Funkcje, które wspomagają pracę z programami linii InsERT GT:</h4>

	<ul>
		<li>zbiorcze naliczanie i wydruk deklaracji skarbowych oraz ich elektroniczną wysyłkę;</li>
		<li>zbiorcze naliczanie i eksport deklaracji ZUS do pliku XML, które są następnie wczytywane przez program Płatnik;</li>
		<li>wydruk księgi przychodów i rozchodów oraz ewidencji VAT;</li>
		<li>zbiorcze archiwizowanie i dearchiwizowanie baz danych podmiotów obsługiwanych przez biuro rachunkowe;</li>
		<li>zbiorcza konwersja baz danych podmiotów obsługiwanych przez biuro rachunkowe;
		ustawianie wybranych parametrów administracyjnych dla wielu podmiotów (kopiowanie wzorców wydruku, kartotek instytucji US i ZUS i Innych).</li>
	</ul>

	<h4>Funkcje ułatwiające zarządzanie biurem rachunkowym:</h4>

	<ul>
		<li>zarządzanie listą podmiotów obsługiwanych przez biuro</li>
		<li>zarządzanie personelem i uprawnieniami;</li>
		<li>zaawansowane opisywanie podmiotów za pomocą definiowalnych procesów.;</li>
	</ul>	
	</div>

</div>