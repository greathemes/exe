<section class="help section">

	<div class="container">
		<header class="section-header">
			<h3>Zdalne wsparcie</h3>
			<h4>Potrzebujesz pomocy? Śmiało!</h4>
		</header>

		<h5>Skorzystaj z programu TeamViewer! Siedzisz wygodnie w fotelu, my wchodzimy na Twój komputer przez internet i naprawiamy usterkę.</h5>

			<div class="instruction-link">
				<a href="https://get.teamviewer.com/qf4pss4" target="_blank">Pobierz Teamviever</a>
			</div>		
			<p class="instruction-phone">Jeżeli masz problem, zadzwoń na numer <span>(56) 64 31 500</span> - pomożemy!</p>	
	</div>

</section>