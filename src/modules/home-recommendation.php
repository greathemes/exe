<section class="recommendation section-light">

	<div class="container">
		<header class="section-header-light">
			<h3>Polecamy</h3>
			<h4>Programy i strony warte uwagi</h4>
		</header>

		<div class="recommendation-links">
			<a href="https://www.insert.com.pl/programy_dla_firm.html" target="_blank">
				<img src="img/insert.png" alt="Logo firmy Insert">		
				<p>Przejdź do <span>Insert</span></p>		
			</a>
			<a href="http://www.novitus.pl/" target="_blank">
				<img src="img/novitus.jpg" alt="Logo firmy Novitus">
				<p>Przejdź do <span>Novitus</span></p>	
			</a>
			<a href="http://www.wapro.pl/WAPRO" target="_blank">
				<img src="img/wapro.gif" alt="Logo firmy Wapro">
				<p>Przejdź do <span>Wapro</span></p>	
			</a>					
			<a href="https://get.teamviewer.com/qf4pss4" target="_blank">
				<img src="img/teamviewer.svg" alt="Logo Teamviewer">
				<p>Przejdź do <span>Teamviever</span></p>	
			</a>
			<a href="http://www.insoft.com.pl/pc-market" target="_blank">
				<img src="img/insoft.jpg" alt="Logo Insoft">
				<p>Przejdź do <span>Insoft</span></p>	
			</a>
			<a href="https://www.eltrox.pl/" target="_blank">
				<img src="img/eltrox.png" alt="Logo firmy Eltrox">
				<p>Przejdź do <span>Eltrox</span></p>	
			</a>
		</div>
	</div>

</section>