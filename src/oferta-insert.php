<?php 

$activePage = "insert"; 
require 'modules/head.php';  
require 'header.php'; 

?>

<section class="insert section">

	<div class="container">
		<header class="section-header">
			<h3>Produkty InsERT</h3>
			<h4>Lorem ipsum dolor sit amet.</h4>
		</header>
	</div>

	<div class="container">
		<ul class="insert-lists">
			<li class="insert-list insert-list-active"><button>Rachmistrz GT</button></li>
			<li class="insert-list"><button>Subiekt GT</button></li>
			<li class="insert-list"><button>Gestor GT</button></li>
			<li class="insert-list"><button>Gratyfikant GT</button></li>
			<li class="insert-list"><button>Biuro GT</button></li>
			<li class="insert-list"><button>Rewizor GT</button></li>
			<li class="insert-list"><button>Subiekt nexo</button></li>
			<li class="insert-list"><button>Subiekt nexo PRO</button></li>
		</ul>

		<ul class="insert-items">
			<li class="insert-item insert-item-active"><?php require 'modules/insert/rachmistrz-gt.php'; ?></li>
			<li class="insert-item"><?php require 'modules/insert/subiekt-gt.php'; ?></li>
			<li class="insert-item"><?php require 'modules/insert/gestor-gt.php'; ?></li>
			<li class="insert-item"><?php require 'modules/insert/gratyfikant-gt.php'; ?></li>
			<li class="insert-item"><?php require 'modules/insert/biuro-gt.php'; ?></li>
			<li class="insert-item"><?php require 'modules/insert/rewizor-gt.php'; ?></li>
			<li class="insert-item"><?php require 'modules/insert/subiekt-nexo.php'; ?></li>
			<li class="insert-item"><?php require 'modules/insert/subiekt-nexo-pro.php'; ?></li>
		</ul>
	</div>

</section>

<?php 

require 'modules/home-help.php';  
require 'footer.php'; 

?>