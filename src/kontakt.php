<section class="contact section" id="contact-section">
	<div class="container form-container">
		<button class="hide-contact" id="hide-contact">Schowaj</button>
		<h4>Dane kontaktowe:</h4>

		<address>
			<div class="address-tels">
				<p><span>ul.</span> Józefa Włodka 16B, Grudziądz</p>
				<p><span>tel.:</span> (56) 64 31 500</p>
				<p><span>tel.kom.:</span> 508 282 511</p>
				<p><span>e-mail:</span> salonexe@gmail.com</p>
			</div>
			<div class="address-hours">
				<p>pon.-piąt.: <span>10.00 – 18.00</span></p>
				<p>sobota: <span>10.00 – 13.00</span></p>
				<p>niedziela: <span>Nieczynny</span></p>
			</div>
		</address>
				
	</div>
</section>




