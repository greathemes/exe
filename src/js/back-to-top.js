(function() {

	"use strict";

	const toTop = document.querySelector('#back-to-top');

	function animateScroll() {
		if (document.body.scrollTop > 0 || window.pageYOffset > 0) {
			window.scrollBy(0, -25);
			setTimeout(animateScroll, 5);
		}
	}

	toTop.addEventListener("click", function() {
		animateScroll();
	});

	window.addEventListener("scroll", function() {
		if(document.body.scrollTop >= 550 || window.pageYOffset >= 550 ) {
			toTop.classList.remove("back-to-top-hidden");
			toTop.classList.add("back-to-top-animate");
		} else {
			toTop.classList.remove("back-to-top-animate");
		}
	}, false);

}());