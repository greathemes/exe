(function() {
  'use strict';

  const showContact = document.querySelector('#show-contact'),
  navShowContact = document.querySelector('#nav-show-contact'),
  footerShowContact = document.querySelector('#footer-show-contact'),
  emailShowContact = document.querySelector('.email-show-contact'),
  emailShowContact2 = document.querySelector('.email-show-contact2'),
  hideContact = document.querySelector('#hide-contact'),
  contactSection = document.querySelector('#contact-section');

  function show_contact() {
    document.querySelector('.contact').classList.add('contact-visible');
  }
  

  if (showContact) {
    showContact.addEventListener('click', function() {
      show_contact();
    })
  }

  emailShowContact.addEventListener('click', function() {
    show_contact();
  })
  emailShowContact2.addEventListener('click', function() {
    show_contact();
  })

  navShowContact.addEventListener('click', function() {
    show_contact();
  })

  footerShowContact.addEventListener('click', function() {
    show_contact();
  })

  hideContact.addEventListener('click', function() {
    document.querySelector('.contact').classList.remove('contact-visible');
  })

  const contactFormSubmit = document.querySelector('#contact-form-submit'),
  contactFormStatus = document.querySelector('#contact-form-status');

  document.addEventListener( 'keydown', function( event ) {

  if ( event.keyCode == 27 ) { 

    document.querySelector('.contact').classList.remove('contact-visible');

  }

} );

})();