(function() {
	const mainNav = document.querySelector("#main-nav"),
	 			showMainNav = document.querySelector("#main-header-show-nav-btn"),
	 			hideMainNav = document.querySelector("#hide-main-nav"),
	 			mobileViewport = window.matchMedia("screen and (min-width: 500px)");

	mobileViewport.addListener(function(mq) {

    if (!mobileViewport.matches && mainNav.classList.contains("main-nav-active")) {
        document.body.style.overflowY = "hidden";
    } else {
        document.body.style.overflowY = "auto";
    }

});

	function showNav() {
		mainNav.style.display = "flex";
		showMainNav.style.opacity = 0;
		setTimeout(function() {
			mainNav.classList.add("main-nav-active");
		}, 50);
		if(!mobileViewport.matches) {
			document.body.style.overflowY = "hidden";
		} 
	}

	function hideNav() {
		mainNav.classList.remove("main-nav-active");
		showMainNav.style.opacity = 1;
		setTimeout(function() {
			mainNav.style.display = "none";
		}, 300);
		document.body.style.overflowY = "auto";
	}

	showMainNav.addEventListener("click", showNav);
	hideMainNav.addEventListener("click", hideNav);


  document.addEventListener( 'keydown', function( event ) {

  if ( event.keyCode == 27 ) { 

    hideNav();

  }

} );

})();