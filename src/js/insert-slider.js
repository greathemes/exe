(function() { if(document.body.classList.contains('insert')) {

	const insertList = document.querySelectorAll('.insert-list'),
				insertListArr = [],
				insertItems = document.querySelector('.insert-items'),
				insertItem = document.querySelectorAll('.insert-item'),
				insertItemArr = [];

	for ( i = 0; i < insertList.length; i++ ) { 

    insertListArr.push( insertList[i] );

	}

	for ( i = 0; i < insertList.length; i++ ) { 

    insertItemArr.push( insertItem[i] );

	}

	

	let activeIndex = 0;

	function setActiveSlide(that) {
		let currentActiveSlide = document.querySelector('.insert-list-active');
		currentActiveSlide.classList.remove('insert-list-active');

		that.classList.add('insert-list-active');
		currentActiveSlide = document.querySelector('.insert-list-active');	

		return insertListArr.indexOf(that);
	}

	function setSlideHeight(i = activeIndex) {
		insertItems.style.height = insertItemArr[i].clientHeight + 'px';	
	}

	setSlideHeight();

	function showSlide(i) {
		let currentActiveSlide = document.querySelector('.insert-item-active');
		currentActiveSlide.classList.remove('insert-item-active');

		insertItemArr[i].classList.add('insert-item-active');
		setSlideHeight(i);
	}
	

	insertListArr.forEach(function(list) {
		list.addEventListener('click', function() {
			const activeIndex = setActiveSlide(this);
			showSlide(activeIndex);
		}, true);
	})

	window.addEventListener('resize', function() {
		setSlideHeight();
	}, true);
}})();