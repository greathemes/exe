<button class="back-to-top" id='back-to-top'><i class="fas fa-arrow-up"></i></button>

<footer class="main-footer">
	<div class="container">
		<ul class="footer-menu">
			<li><a href="./" <?php echo ($activePage == 'home') ? 'class="active-footer-link"' : ''; ?>>Strona główna</a></li>
			<li><a href="./o-nas.php" <?php echo ($activePage == 'about') ? 'class="active-footer-link"' : ''; ?>>O nas / Oferta</a></li>
			<li><a href="./oferta-insert.php" <?php echo ($activePage == 'insert') ? 'class="active-footer-link"' : ''; ?>>Oferta InsERT</a></li>
			<li><a id="footer-show-contact" href="javascript:;">Kontakt</a></li>								
		</ul>
		<div class="footer-adverts-container">
			<?php if( rand(0,1) ) : ?>
				<div align="center"><a href="https://www.insert.com.pl/subiekt_gt" target="_blank"><img src="http://bannery.insert.com.pl/subiekt_gt_rectangle.png" alt="Subiekt GT: Nowoczesny i przyjazny dla użytkownika system sprzedaży z obsługą magazynów" border="0"></a></div>
				<div align="center"><a href="https://www.insert.com.pl/subiekt_gt" target="_blank"><img src="http://bannery.insert.com.pl/subiekt_gt_doublebill.png" alt="Subiekt GT: Nowoczesny i przyjazny dla użytkownika system sprzedaży z obsługą magazynów" border="0"></a></div>
				<?php else : ?>
					<div align="center"><a href="https://www.insert.com.pl/rachmistrz_gt" target="_blank"><img src="http://bannery.insert.com.pl/rachmistrz_gt_rectangle.png" alt="Rachmistrz GT: Księga przychodów i rozchodów oraz system naliczania podatku zryczałtowanego" border="0"></a></div>
					<div align="center"><a href="https://www.insert.com.pl/rachmistrz_gt" target="_blank"><img src="http://bannery.insert.com.pl/rachmistrz_gt_doublebill.png" alt="Rachmistrz GT: Księga przychodów i rozchodów oraz system naliczania podatku zryczałtowanego" border="0"></a></div>				
				<?php endif; ?>			
			</div>
		</div>
		<div class="outer-container footer-description">
			<span>&copy; Copyright <?php echo date("Y"); ?> Exe</span>
			<a class="cert" href="img/certyfikat.pdf"><img src="img/certyfikat.png"></a>
		</div>
	</footer>
	<script src="js/app-min.js"></script>
	<?php if( $activePage == 'test' ) : ?>
		<script>
			var map,
			popup = document.querySelector( '.alert-popup' ),
			close_btn = document.querySelector( '#hide-alert-popup' );
			function initMap() {
				map = new google.maps.Map(
					document.getElementById( 'alert-popup-map' ),
					{
						center: {
							lat: 53.483564,
							lng: 18.749807
						},
						zoom: 19,
						mapTypeId: google.maps.MapTypeId.HYBRID,
						labels: true,
					}
				);
			}

			close_btn.addEventListener( 'click',  function() {
				popup.classList.add( 'inactive' );
			}, false );
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBUpBEzLtFIQE1D9YIhRtJSrvFAedtRnkE&callback=initMap&language=PL" async defer></script>
	<?php endif; ?>

	<?php echo $activePage == "insert" ? '<script src="js/insert-slider-min.js"></script>' : '' ?>
</body>
</html>
