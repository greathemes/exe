<?php 

$activePage = "about"; 
require 'modules/head.php';  
require 'header.php'; 

?>

<section class="company section-light">

	<div class="container">
		<header class="section-header">
			<h3>O firmie</h3>
			<h4>Kilka słów o nas.</h4>
		</header>
	</div>

	<div class="container">
		<p>Rzetelni, profesjonalni i stawiający sobie dobro klienta jako najważniejsze. Proponujemy najnowsze produkty i rozwiązania informatyczne renomowanych producentów wraz z możliwością ich wdrożenia. Oferujemy atrakcyjne ceny, wysoki poziom jakościowy. Gwarantujemy, że otrzymacie państwo sprzęt na miarę własnych potrzeb.</p>
		<div class="company-content">
			<a href="https://www.eltrox.pl/" target="_blank">
				<img src="img/eltrox.png" alt="Logo firmy Eltrox">
			</a>
			<p>Celem podniesienia jakości naszych usług współpracujemy z najlepszymi. Jednym z naszych partnerów jest firma <a href="https://www.eltrox.pl/" target="_blank">Eltrox</a>. Doskonale zaopatrzony internetowy sklep elektroniczny, ale także sieć punktów sprzedaży stacjonarnej. Cennik oraz pełną ofertę usług można znaleźć <a href="https://www.eltrox.pl/" target="_blank">pod tym linkiem</a>.</p> 
		</div>
	</div>

</section>

<section class="repair section-light">
	<i class="icon-chip"></i>
	<div class="container">
		<header class="section-header-light">
			<h3>Co robimy?</h3>
			<h4>Jeśli czegoś brak, dzwoń! Rozwijamy się na bieżąco.</h4>
		</header>
	</div>

	<div class="container offer-container">
		<div>
			<h5>Oferta</h5>
	
			<ul>
				<li>Zestawy komputerowe</li>
				<li>Notebooki, netbooki, tablety</li>
				<li>Podzespoły do komputerów stacjonarnych i przenośnych</li>
				<li>Nośniki danych i materiały eksploatacyjne</li>
				<li>Peryferia komputerowe</li>
				<li>Drukarki, skanery, projektory multimedialne</li>
				<li>Akcesoria i osprzęt do sieci LAN i WLAN</li>			
				<li>Oprogramowanie firm Insert (Subiekt, Rachmistrz, itd.) <a href="https://www.insert.com.pl/index.html" target="_blank">www.insert.pl</a></li>	
				<li>Oprogramowanie firm Asseco (Wapro, MAG, Kaper, itd.) <a href="http://www.wapro.pl/wapro" target="_blank">www.wapro.pl</a></li>
				<li>Urządzenia / systemy fiskalne Novitus <a href="https://www.novitus.pl/" target="_blank">www.novitus.pl</a></li>
				<li>Terminale płatnicze <a href="http://www.paytel.pl/" target="_blank">www.paytel.pl</a></li>
			</ul>		
		</div>
		<div>
			<h5>Serwis</h5>
				<!-- <img src="img/laptop.png"> -->
			<img src="img/electronic.png">

			<p>Profesjonalny serwis gwarancyjny i pogwarancyjny:</p>
			<ul>
				<li>Sprzętu komputerowego</li>
				<li>Drukarek i urządzeń wielofunkcyjnych laserowych</li>
				<li>Systemów wizyjnych i monitorów LCD</li>
				<li>Systemów nawigacji GPS</li>
				<li>Urządzeń fiskalnych</li>
				<li>Infrastruktury sieci LAN</li>
				<li>Laptopów, netbooków, tabletów</li>
				<li>Infrastruktury i urządzeń alarmowych</li>
				<li>Oprogramowania firmy Insert (Rachmistrz, Subiekt, itd.)</li>
				<li>Oprogramowania firmy Asseco (Płatnik, WAPRO - MAG/KAPER, itd.)</li>
			</ul>
		</div>
		<div>
			<h5>Usługi</h5>
			<!-- <img src="img/laptop.png"> -->
			<!-- <img src="img/pc.png"> -->
			<ul>
				<li>Pomoc przy doborze i konfiguracji sprzętu komputerowego</li>
				<li><span>Darmowa</span> diagnostyka sprzętu komputerowego / elektronicznego</li>
				<li>Audyt licencyjny oprogramowania komputerowego</li>
				<li>Outsourcing informatyczny w firmach</li>
				<li>Odzyskiwanie danych z uszkodzonych nośników</li>
				<li>Naprawa elektroniki po zalaniu / przepięciu, itp.</li>
				<li>Wymiana uszkodzonych elementów elektronicznych (gniazda, elektronika SMD, itp.)</li>
				<li>Wsparcie techniczne dla produktów zakupionych w innych firmach</li>
			</ul>
		</div>
	</div>

	<div class="container link-container">
		<a href="./oferta-insert.php">Zobacz ofertę firmy InsERT</a>
	</div>

</section>

<?php 

require 'modules/home-help.php';
require 'footer.php'; 