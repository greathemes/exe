<header class='main-header <?php echo $activePage == "home" ? 'home' : ''; ?>'>
	<div class='outer-container'>
		<div class="main-header-menu container">
			
			<?php if( $activePage == 'home' ) : ?>
				<h2><a class='main-header-logo'>Exe</a></h2>
				<?php else : ?>
					<h2><a class='main-header-logo' href='./'>Exe</a></h2>
				<?php endif; ?>		

				<div>
					<a target="_blank" href="https://get.teamviewer.com/qf4pss4" class="main-header-show-tv-btn">Teamviewer</a>
					<button id='main-header-show-nav-btn' class='main-header-show-nav-btn'>Menu</button>
				</div>

				<address class="main-header-contact">
					<span class="email-show-contact"><strong>e-mail: </strong>salonexe@gmail.com</span>
					<span><strong>tel.: </strong>(56) 64 31 500</span>			
				</address>
				
			</div>
		</div>

		<?php if( $activePage == 'home' ) : ?>
			<div class="container main-header-content">
				<h1>Witaj w EXE!</h1>
				<p>Posiadamy salon komputerowy i świadczymy usługi i rozwiązania informatyczne <span>najwyższej</span> jakości.</p>
				<span class="divider"></span>
				<button id="show-contact">Skontaktuj się z nami</button>
			</div>
			<?php endif; ?>

		</header>

		<?php 
		require 'modules/main-navigation.php'; 
		require 'kontakt.php';
		?>