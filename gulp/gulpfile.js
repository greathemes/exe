const gulp = require("gulp");
const runSequence = require("run-sequence");
const requireDir = require('require-dir');
const tasks = requireDir('./tasks', {recurse: false});

gulp.task('default', function(callback) {
	runSequence('clean',
		["script", "script-slider", "sass", "php", "img", "browserSync", "watch"],
		callback);
});

gulp.task('build', function(callback) {
	runSequence('clean',
		["script", "sass", "php", "img"],
		callback);
});
