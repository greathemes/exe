const gulp        = require("gulp");
const browserSync = require("browser-sync");

gulp.task("reload", ['script'], function(done) {
	browserSync.reload();
	done();
});

gulp.task("reload-slider", ['script-slider'], function(done) {
	browserSync.reload();
	done();
});