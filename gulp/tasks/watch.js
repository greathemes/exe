const config      = require("../config");
const gulp        = require("gulp");
const runSequence = require("run-sequence");
const browserSync = require("browser-sync");

gulp.task("watch", function() {
	gulp.watch(config.views.src, ["php"]);
	gulp.watch(config.styles.src, ["sass"]);
	gulp.watch(config.scripts.src + '**/*.js', ["script", "reload"]);
	gulp.watch(config.scripts.src + 'insert-slider.js', ["script-slider", "reload-slider"]);
});