const config       = require("../config");
const gulp         = require("gulp");
const sass         = require("gulp-sass");
const browserSync  = require("browser-sync");
const autoprefixer = require("gulp-autoprefixer");
const bulkSass     = require('gulp-sass-glob-import');
const sourcemaps   = require('gulp-sourcemaps');
const cleanCSS     = require('gulp-clean-css');
const cssnano      = require('gulp-cssnano');
const concatCss    = require('gulp-concat-css');

gulp.task("sass", function() {
  return gulp.src(config.styles.src)
  .pipe(bulkSass())
  .pipe(sourcemaps.init())
  .pipe(sass({ 
    'errLogToConsole': true 
  })
  .on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['last 3 versions']
  }))
  .pipe(sourcemaps.write())
  .pipe(concatCss("main-min.css"))
  .pipe(cleanCSS())
  .pipe(gulp.dest(config.styles.dest))
  .pipe(browserSync.stream());
});

// gulp.task("sass", function() {
//   return gulp.src(config.styles.src)
//   // .pipe(bulkSass())
//   .pipe(sass())
//   .pipe(concatCss("main-min.css"))
//   // .pipe(cleanCSS())
//   .pipe(cssnano())
//   .pipe(gulp.dest(config.styles.dest))
//   .pipe(browserSync.stream());
// });
