const config = require("../config");
const gulp   = require("gulp");
const image  = require('gulp-image');

gulp.task('img', function() {
	return gulp.src(config.images.src)
	.pipe(gulp.dest(config.images.dest))
});
