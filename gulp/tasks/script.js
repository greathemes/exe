const config      = require("../config");
const gulp        = require("gulp");
const minify      = require('gulp-minify');
const uglify      = require('gulp-uglify');
const concat      = require('gulp-concat');
const browserSync = require("browser-sync");

gulp.task('script', function() {
	gulp.src([config.scripts.src + '**/*.js', '!' + config.scripts.src + 'insert-slider.js'])
	.pipe(concat('app.js'))
	.pipe(minify())
	.pipe(gulp.dest(config.scripts.dest))
	.pipe(browserSync.stream());
});

gulp.task('script-slider', function() {
	gulp.src(config.scripts.src + 'insert-slider.js')
	.pipe(minify())
	.pipe(gulp.dest(config.scripts.dest))
	.pipe(browserSync.stream());
});