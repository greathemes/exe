const config      = require("../config");
const gulp        = require("gulp");
const browserSync = require("browser-sync");

gulp.task("browserSync", function() {
	browserSync.init({
		proxy: 'localhost:8888/' + config.proxy,
	});
});