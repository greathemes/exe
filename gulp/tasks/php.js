const config      = require("../config");
const gulp        = require("gulp");
const connect     = require("gulp-connect-php");
const browserSync = require("browser-sync");

gulp.task('php', function() {
	return gulp.src(config.views.src)
	.pipe(gulp.dest(config.views.dest))
	.pipe(browserSync.stream());
});