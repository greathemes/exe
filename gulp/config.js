module.exports = {
  proxy: 'exe/build',

  myPath: {
    src: "../src",
    dest: "../build"
  },

  scripts: {
    src: './../src/js/',
    dest: '../build/js'
  },

  styles: {
    src: "../src/sass/**/*.scss",
    dest: '../build/css'
  },

  views: {
    src: "../src/**/*.php",
    dest: "../build"
  },
  images: {
    src: '../src/img/**',
    dest: "../build/img"
  },
};
